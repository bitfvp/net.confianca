<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{

    }
}

$page="Editar cab-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");

if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $a="cabsave";
    $cab=fncgetcab($_GET['id']);

}else{
    $a="cabnew";
}
?>
<div class="container-fluid"><!--todo conteudo-->
    <form class="form-signin" action="<?php echo "index.php?pg=Vcab_editar&aca={$a}"; ?>" method="post" id="formx">
        <h3 class="form-cadastro-heading">Cadastro</h3>
        <hr>
        <div class="row">
            <div class="col-md-12">
                <input id="id" type="hidden" class="txt bradius" name="id" value="<?php echo $cab['id']; ?>"/>
                <label for="empresa">EMPRESA</label>
                <input autocomplete="off" autofocus id="empresa" placeholder="" type="text" class="form-control" name="empresa" value="<?php echo $cab['empresa']; ?>" required/>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <label for="linha1">LINHA 1</label>
                <input autocomplete="off" id="linha1" type="text" class="form-control" name="linha1" value="<?php echo $cab['linha1']; ?>" placeholder="RUA PRINCIPAL, 555 - BAIRRO CENTRO - CIDADE/UF TELEFONE:(99)9999-9999"/>
            </div>
            <div class="col-md-12">
                <label for="linha2">LINHA 2</label>
                <input autocomplete="off" id="linha2" type="text" class="form-control" name="linha2" value="<?php echo $cab['linha2']; ?>" placeholder="CNPJ 00.000.000/0000-00 - EMAIL corretora@email.com"/>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-12">
                <input type="submit" name="gogo" id="gogo" class="btn btn-lg btn-success btn-block my-2" value="SALVAR"/>
            </div>
            <script>
                var formID = document.getElementById("formx");
                var send = $("#gogo");

                $(formID).submit(function(event){
                    if (formID.checkValidity()) {
                        send.attr('disabled', 'disabled');
                        send.attr('value', 'AGUARDE...');
                    }
                });
            </script>
        </div>
    </form>
    </div>
    </form>

</main>

<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>