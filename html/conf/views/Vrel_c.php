<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{

    }
}

$page="Relatório de fechamentos por comprador-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
?>
<main class="container"><!--todo conteudo-->
    <div class="row">
        <div class="col-md-6 offset-md-3">
    <!-- =============================começa conteudo======================================= -->
            <div class="card">
                <div class="card-header bg-info text-light">
                    Relatório de fechamentos por comprador
                </div>
                <div class="card-body">
                        <form action="index.php?pg=Vrel_cprint" method="post" target="_blank">



                            <div class="form-group">
                                <label for="comprador">Comprador:</label>
                                <select name="comprador" id="comprador" class="form-control" required>
                                    <option selected="" value="">
                                        Selecione...
                                    </option>
                                    <?php
                                    foreach(fncpessoalist() as $item){
                                        ?>
                                        <option value="<?php echo $item['id']; ?>"><?php echo $item['nome']; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>

                        <div class="form-group">
                        <label for="data_inicial">Data Inicial:</label>
                            <input id="data_inicial" type="date" class="form-control" name="data_inicial" value="<?php echo date("Y-m-01");?>" required/>
                        </div>

                        <div class="form-group">
                        <label for="data_final">Data Final:</label>
                            <input id="data_final" type="date" class="form-control" name="data_final" value="<?php echo date("Y-m-t");?>" required/>
                        </div>

                            <div class="form-group">
                                <label for="cab">CABEÇALHO:</label>
                                <select name="cab" id="cab" class="form-control input-sm" data-live-search="true">

                                    <option selected="" data-tokens="" value="0">
                                        PADRÃO
                                    </option>
                                    <?php
                                    foreach (fnccablist() as $item) {
                                        ?>
                                        <option data-tokens="<?php echo $item['empresa'];?>" value="<?php echo $item['id'];?>">
                                            <?php echo $item['empresa']; ?>
                                        </option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>


                            <div class="form-group">
                                <label for="corretor">Corretor:</label>
                                <select name="corretor" id="corretor" class="form-control">
                                    <option selected="" value="0">
                                        Selecione...
                                    </option>
                                    <?php
                                    foreach(fnccorretorlist() as $item){
                                        ?>
                                        <option value="<?php echo $item['id']; ?>"><?php echo $item['corretor']; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            <input type="submit" class="btn btn-lg btn-success btn-block mb-2" value="GERAR RELATÓRIO"/>
                    </form>
                    </div>
                <div class="card-footer text-warning">Campo corretor em branco para selecionar todos.</div>
                </div>

    <!-- =============================fim conteudo======================================= -->
        </div>
    </div>
</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>