<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
//        if ($allow["allow_9"]!=1){
//            header("Location: {$env->env_url}?pg=Vlogin");
//            exit();
//        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
echo"<META HTTP-EQUIV=REFRESH CONTENT = '3000;URL={$env->env_url_mod}index.php?pg=Vfechamento_lista'>";
include_once("includes/topo.php");

try{
    $sql = "SELECT "
        ."conf_fechamentos.id, "
        ."conf_fechamentos.`status`, "
        ."conf_fechamentos.data_ts, "
        ."conf_fechamentos.corretagem_c, "
        ."conf_fechamentos.corretagem_v, "
        ."conf_fechamentos.ordem_compra, "
        ."conf_fechamentos.descarga, "
        ."conf_fechamentos.descricao, "
        ."conf_fechamentos.condicao_pag, "
        ."conf_fechamentos.condicao_entrega, "
        ."conf_fechamentos.prazo_entrega, "
        ."conf_fechamentos.forma_pag, "
        ."conf_fechamentos.observacao, "
        ."pessoas_comprador.nome AS comprador, "
        ."pessoas_vendedor.nome AS vendedor "
        ."FROM "
        ."conf_fechamentos "
        ."INNER JOIN conf_pessoas AS pessoas_comprador ON pessoas_comprador.id = conf_fechamentos.comprador "
        ."INNER JOIN conf_pessoas AS pessoas_vendedor ON pessoas_vendedor.id = conf_fechamentos.vendedor "
        ."WHERE "
        ."conf_fechamentos.id <> 0 ";

    if (isset($_GET['sca']) and $_GET['sca']!='') {
        $sca=$_GET['sca'];
        $sql .=" AND pessoas_comprador.nome LIKE '%$sca%' ";
    }

    if (isset($_GET['scb']) and $_GET['scb']!='') {
        $scb=$_GET['scb'];
        $sql .=" AND pessoas_vendedor.nome LIKE '%$scb%' ";
    }

    if (isset($_GET['scc']) and $_GET['scc']!=0 and $_GET['scc']!='') {
        $inicial=$_GET['scc'];
        $inicial.=" 00:00:01";
        $final=$_GET['scc'];
        $final.=" 23:59:59";
        $sql .=" AND ((conf_fechamentos.data_ts)>=:inicial) And ((conf_fechamentos.data_ts)<=:final) ";
    }else{
        if (((isset($_GET['sca']) and $_GET['sca']!='') or (isset($_GET['scb']) and $_GET['scb']!='')) or ((isset($_GET['scc']) and $_GET['scc']!='') or (isset($_GET['scc']) and $_GET['scc']!=''))){
            //tempo todo
            $inicial="2021-01-01";
            $inicial.=" 00:00:01";
            $final=date("Y-m-d");;
            $final.=" 23:59:59";
            $sql .=" AND ((conf_fechamentos.data_ts)>=:inicial) And ((conf_fechamentos.data_ts)<=:final) ";
        }else{
            //apenas dia anterior
            $inicial=date("Y-m-d",strtotime("-30 days"));
            $inicial.=" 00:00:01";
            $final=date("Y-m-d");;
            $final.=" 23:59:59";
            $sql .=" AND ((conf_fechamentos.data_ts)>=:inicial) And ((conf_fechamentos.data_ts)<=:final) ";
        }

    }

    $sql .="order by conf_fechamentos.data_ts DESC LIMIT 0,50 ";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindValue(":inicial", $inicial);
    $consulta->bindValue(":final", $final);
//    if (isset($_GET['sca']) and  is_numeric($_GET['sca']) and $_GET['sca']!=0) {
//        $consulta->bindValue(":romaneio", "%".$_GET['sca']."%");
//    }
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
}catch ( PDOException $error_msg){
    echo 'Erro'. $error_msg->getMessage();
}

$entradas = $consulta->fetchAll();
$entradas_quant = $consulta->rowCount();
$sql = null;
$consulta = null;
?>

<main class="container"><!--todo conteudo-->
    <h3 class="form-cadastro-heading">FECHAMENTOS</h3>
    <hr>

    <form action="index.php" method="get">
        <div class="input-group mb-3 col-md-9 float-left">
            <div class="input-group-prepend">
                <button class="btn btn-outline-success" type="submit"><i class="fa fa-search animated swing infinite"></i></button>
            </div>
            <input name="pg" value="Vfechamento_lista" hidden/>
            <input type="text" name="sca" id="sca" placeholder="comprador" autocomplete="off" class="form-control" value="<?php if (isset($_GET['sca'])) {echo $_GET['sca'];} ?>" />
            <input type="text" name="scb" id="scb" placeholder="vendedor" autocomplete="off" class="form-control" value="<?php if (isset($_GET['scb'])) {echo $_GET['scb'];} ?>" />
            <input type="date" name="scc" id="scc" autocomplete="off" class="form-control" value="<?php if (isset($_GET['scc'])) {echo $_GET['scc'];} ?>" />
        </div>
    </form>

    <script type="text/javascript">
        function selecionaTexto()
        {
            document.getElementById("sca").select();
        }
        window.onload = selecionaTexto();
    </script>

    <div class="row">
        <div class="col-md-12">
            <a href="index.php?pg=Vfechamento_editar" class="btn btn-block btn-primary mb-2" ><i class="fas fa-level-down-alt"></i> Novo</a>
        </div>
    </div>

    <table class="table table-sm table-stripe table-hover table-bordered">
        <thead class="thead-dark">
        <tr>
            <!--            <th scope="col" class="text-center">#</th>-->
            <th scope="col"><small>NR</small></th>
            <th scope="col"><small>ORD. DE COMPRA</small></th>
            <th scope="col"><small>EMISSÃO</small></th>
            <th scope="col"><small>COMPRADOR</small></th>
            <th scope="col"><small>VENDEDOR</small></th>
            <th scope="col" class="text-center"><small>AÇÕES</small></th>
        </tr>
        </thead>
        <tfoot>
        <tr class="bg-warning">
            <th colspan="4" class="bg-info text-right"></th>
            <th colspan="2" class="bg-info text-right"><?php echo $entradas_quant;?> Entrada(s) encontrada(s)</th>
        </tr>
        </tfoot>

        <tbody>
        <script>
            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            })
        </script>
        <?php
        if($_GET['sca']!="" and isset($_GET['sca'])) {
            $sta = strtoupper($_GET['sca']);
            define('CSA', $sta);
        }
        if($_GET['scb']!="" and isset($_GET['scb'])) {
            $stb = strtoupper($_GET['scb']);
            define('CSB', $stb);
        }
        foreach ($entradas as $dados){
            $fe_id = $dados["id"];
            $ordem_compra = $dados["ordem_compra"];
            $comprador = strtoupper($dados["comprador"]);
            $vendedor = strtoupper($dados["vendedor"]);
            $data_ts = dataRetiraHora($dados["data_ts"]);
            $usuario = $dados["usuario"];

            if ($dados["datahora_saida"]=="" or $dados["datahora_saida"]==0){
                $corlinha="";
            }else{
                $corlinha=" bg-dark text-warning ";
            }

            ?>

            <tr id="<?php echo $fe_id;?>" class="<?php echo $corlinha;?>">
                <td><?php echo utf8_encode(strftime('%Y', strtotime("{$dados['data_ts']}")))."-".$dados['id']; ?></td>
                <td><?php echo strtoupper($ordem_compra); ?></td>
                <td><?php echo $data_ts; ?></td>
                <td><?php
                    if($_GET['sca']!="" and isset($_GET['sca'])) {
                        $sta = CSA;
                        $aaa = $comprador;
                        $aa = explode(CSA, $aaa);
                        $a = implode("<span class='text-danger'>{$sta}</span>", $aa);
                        echo $a;
                    }else{
                        echo $comprador;
                    }
                    ?>
                </td>

                <td><?php
                    if($_GET['scb']!="" and isset($_GET['scb'])) {
                        $stb = CSB;
                        $aaa = $vendedor;
                        $aa = explode(CSB, $aaa);
                        $a = implode("<span class='text-danger'>{$stb}</span>", $aa);
                        echo $a;
                    }else{
                        echo $vendedor;
                    }
                    ?>
                </td>
                <td class="text-center">

                        <div class="btn-group" role="group" aria-label="">
                            <a href="index.php?pg=Vfechamento&id=<?php echo $fe_id; ?>" title="acessar" class="btn btn-sm btn-warning fas fa-search-plus text-dark">
                                <br>ACESSAR
                            </a>


<!--                                <div class="dropdown show">-->
<!--                                    <a class="btn btn-danger btn-sm dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">-->
<!--                                        <i class="fas fa-trash"><br>EXCLUIR</i>-->
<!--                                    </a>-->
<!--                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">-->
<!--                                        <a class="dropdown-item" href="#">Não</a>-->
<!--                                        <a class="dropdown-item bg-danger" href="index.php?pg=Vfechamento_lista&aca=fechamentodelete&tabela_id=--><?php //echo $fe_id; ?><!--">Apagar</a>-->
<!--                                    </div>-->
<!--                                </div>-->


                        </div>

                </td>
            </tr>

            <?php
        }
        ?>
        </tbody>
    </table>


</main>

<?php
include_once("{$env->env_root}includes/footer.php");
?>
</body>
</html>