<?php
//gerado pelo geracode
function fnccorretorlist(){
    $sql = "SELECT * FROM conf_corretor ORDER BY id";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $corretorlista = $consulta->fetchAll();
    $sql = null;
    $consulta = null;
    return $corretorlista;
}

function fncgetcorretor($id){
    $sql = "SELECT * FROM conf_corretor WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1, $id);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $getconf_corretor = $consulta->fetch();
    $sql = null;
    $consulta = null;
    return $getconf_corretor;
}
?>