<table class="table table-sm">
    <thead class="thead-dark">
    <tr>
        <th>Descrição</th>
        <th>Peso</th>
        <th>Sacas</th>
        <th>Preço</th>
        <th>Ação</th>
    </tr>
    </thead>
    <tbody>

    <?php
    try{
        $sql="SELECT * FROM ";
        $sql.="conf_fechamentos_lotes ";
        $sql.="WHERE id_fechamento=:id and status=1";
        global $pdo;
        $consulta=$pdo->prepare($sql);
        $consulta->bindValue(":id", $_GET['id']);
        $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    }catch ( PDOException $error_msg){
        echo 'Erroff'. $error_msg->getMessage();
    }
    $lotes=$consulta->fetchAll();

    $sum=0;
    foreach ($lotes as $dados){?>
        <tr>
            <form action="index.php?pg=Vfechamento&id=<?php echo $_GET['id'];?>&aca=fechamento_loteupdate&id_lote=<?php echo $dados['id'];?>" method="post" id="formx">
                <td>
                    <input type="text" name="descricao" id="descricao" class="form-control form-control-sm" value="<?php echo $dados['descricao'];?>" placeholder="descricao" >
                </td>

                <td>
                    <input type="number" name="peso" id="peso<?php echo $dados['id'];?>" class="form-control form-control-sm" value="<?php echo $dados['peso'];?>" min="0">
                </td>
                <script>
                    jQuery(document).ready(function(){
                        jQuery('#peso<?php echo $dados['id'];?>').on('keyup',function(){
                            var calculoSaca = (jQuery('#peso<?php echo $dados['id'];?>').val() == '' ? 0 : jQuery('#peso<?php echo $dados['id'];?>').val());
                            var calculoSaca2 = (parseFloat(calculoSaca) / 60.5);
                            var textoinfo1 = calculoSaca2 ;
                            jQuery('#sacas<?php echo $dados['id'];?>').val(textoinfo1.toFixed(2));
                        });
                    });
                </script>
                <td>
                    <input type="number" name="sacas" id="sacas<?php echo $dados['id'];?>" class="form-control form-control-sm" value="<?php echo $dados['sacas'];?>" min="0">
                </td>
                <td>
                    <input type="number" name="preco" id="preco" class="form-control form-control-sm" value="<?php echo $dados['preco'];?>" min="0">
                </td>
                <td>

                    <div class="btn-group" role="group" aria-label="">
                        <button type="submit" name="gogo" id="gogo" class="btn btn-sm btn-success">SALVAR</button>


                        <div class="dropdown show">
                            <a class="btn btn-sm btn-danger dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                EXCLUIR
                            </a>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                <a class="dropdown-item" href="#">Não</a>
                                <a class="dropdown-item bg-danger" href="index.php?pg=Vfechamento&aca=fechamento_lotedelete&id=<?php echo $_GET['id']; ?>&tabela_id=<?php echo $dados['id']; ?>">Apagar</a>
                            </div>
                        </div>
                    </div>

                    <script>
                        var formID = document.getElementById("formx");
                        var send = $("#gogo");

                        $(formID).submit(function(event){
                            if (formID.checkValidity()) {
                                send.attr('value', 'AGUARDE...');
                                send.attr('disabled', 'disabled');
                            }
                        });
                    </script>


                </td>
            </form>
        </tr>

        <?php
        $sum=$sum+($dados['sacas']*$dados['preco']);
    }
    ?>


    <tr>
        <form action="index.php?pg=Vfechamento&id=<?php echo $_GET['id'];?>&aca=fechamento_loteinsert" method="post" id="formx">
            <td>
                <input type="text" autofocus name="descricao" id="descricao" class="form-control form-control-sm" placeholder="descricao" >
            </td>
            <td>
                <input type="number" name="peso" id="peso" class="form-control form-control-sm" value="0" min="0">
            </td>
            <script>
                jQuery(document).ready(function(){
                    jQuery('#peso').on('keyup',function(){
                        var calculoSaca = (jQuery('#peso').val() == '' ? 0 : jQuery('#peso').val());
                        var calculoSaca2 = (parseFloat(calculoSaca) / 60.5);
                        var textoinfo1 = calculoSaca2 ;
                        jQuery('#sacas').val(textoinfo1.toFixed(2));
                    });
                });
            </script>
            <td>
                <input type="number" name="sacas" id="sacas" class="form-control form-control-sm" value="0" min="0">
            </td>
            <td>
                <input type="number" name="preco" id="preco" class="form-control form-control-sm" value="0" min="0">
            </td>
            <td>
                <button type="submit" name="gogo" id="gogo" class="btn btn-sm btn-block btn-info">NOVO</button>
                <script>
                    var formID = document.getElementById("formx");
                    var send = $("#gogo");

                    $(formID).submit(function(event){
                        if (formID.checkValidity()) {
                            send.attr('value', 'AGUARDE...');
                            send.attr('disabled', 'disabled');
                        }
                    });
                </script>
            </td>
        </form>
    </tr>



    <tr>
        <td colspan="3"></td>
        <td>
            TOTAL: R$ <?php echo number_format($sum,2);?>
        </td>
    </tr>







    </tbody>

</table>
